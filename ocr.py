import pytesseract as tess
import imutils 
import numpy as np
import cv2
import time as t
import os 
import win32file
import win32event
import win32con
import mysql.connector
from pathlib import Path
from datetime import datetime,timedelta
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
import urllib.parse as url
import DetectChars
import DetectPlates
import PossiblePlate
import math as m
import random 





years = ["1998","1999","2000","2001","2002","2003","2006","2009","2008","2012","2014","2015","2016","2018","2020"]
colors = ["Red","Blue","White","Black","Green","Grey"]
makes = ["Honda","Yamaha","Suzuki","Unique","Road Prince"]
chasis = ["445622","4532459","658269","5486158","515Q55","1W51C531","01I896Y2","195RT22I2","464562FD2Y","U45P3245U9","HG65826UYT9","5NB4V86158","YT51UYTRF5155","1515FGHJ31","01BVXC8962","195NBVC222"]


SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)

showSteps = False


mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="",
  database="precedium"
)
cur = mydb.cursor()



tess.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'




def drawRedRectangleAroundPlate(imgOriginalScene, licPlate):

    p2fRectPoints = cv2.boxPoints(licPlate.rrLocationOfPlateInScene)           

    cv2.line(imgOriginalScene, tuple(p2fRectPoints[0]), tuple(p2fRectPoints[1]), SCALAR_RED, 2)         
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[1]), tuple(p2fRectPoints[2]), SCALAR_RED, 2)
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[2]), tuple(p2fRectPoints[3]), SCALAR_RED, 2)
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[3]), tuple(p2fRectPoints[0]), SCALAR_RED, 2)

def writeLicensePlateCharsOnImage(imgOriginalScene, licPlate):
    ptCenterOfTextAreaX = 0                             
    ptCenterOfTextAreaY = 0

    ptLowerLeftTextOriginX = 0                          
    ptLowerLeftTextOriginY = 0

    sceneHeight, sceneWidth, sceneNumChannels = imgOriginalScene.shape
    plateHeight, plateWidth, plateNumChannels = licPlate.imgPlate.shape

    intFontFace = cv2.FONT_HERSHEY_SIMPLEX  
    fltFontScale = float(plateHeight) / 30.0                    
    intFontThickness = int(round(fltFontScale * 1.5))           

    textSize, baseline = cv2.getTextSize(licPlate.strChars, intFontFace, fltFontScale, intFontThickness)        

            
    ( (intPlateCenterX, intPlateCenterY), (intPlateWidth, intPlateHeight), fltCorrectionAngleInDeg ) = licPlate.rrLocationOfPlateInScene

    intPlateCenterX = int(intPlateCenterX)              
    intPlateCenterY = int(intPlateCenterY)

    ptCenterOfTextAreaX = int(intPlateCenterX)         

    if intPlateCenterY < (sceneHeight * 0.75):                                                  
        ptCenterOfTextAreaY = int(round(intPlateCenterY)) + int(round(plateHeight * 1.6))     
    else:                                                                                       
        ptCenterOfTextAreaY = int(round(intPlateCenterY)) - int(round(plateHeight * 1.6))      


    textSizeWidth, textSizeHeight = textSize                

    ptLowerLeftTextOriginX = int(ptCenterOfTextAreaX - (textSizeWidth / 2))          
    ptLowerLeftTextOriginY = int(ptCenterOfTextAreaY + (textSizeHeight / 2))         


    cv2.putText(imgOriginalScene, licPlate.strChars, (ptLowerLeftTextOriginX, ptLowerLeftTextOriginY), intFontFace, fltFontScale, SCALAR_YELLOW, intFontThickness)




def getCurrentTime():
    now = datetime.now()
    current_time = now.strftime("_%Y%m%d_%H%M%S")
    return current_time

def getCurrentChallanDateTime():
    now = datetime.now()
    current_time = now.strftime("%Y-%m-%d %I:%M:%S %p")
    return current_time

def getDate():
    now = datetime.now()
    current_time = now.strftime("%Y-%m-%d")
    return current_time
def getTime():
    now = datetime.now()
    current_time = now.strftime("%I:%M:%S %p")
    return current_time


def getDueDate():
   now = datetime.today()
   newDate = now + timedelta(15)
   current_time = newDate.strftime("%d-%m-%Y")
   return current_time

def getFileName(full = True):    
    #fileName = "Challan"+getCurrentTime()
    fileName = "MY"
    extention = "pdf"
    fileObj=""
    
    if(full):
        fileObj = fileName+"."+extention
    else:
        fileObj = fileName
    
    return fileObj


def GET_VEHICLE(np):
    sql = "SELECT * FROM vehicledetails where numberPlate = '"+np+"'"
    cur.execute(sql)
    result = cur.fetchone()
    return result
    
def GET_ONWER(cnic):
    sql = "SELECT * FROM vehicleowner where cnic = '"+cnic+"'"
    cur.execute(sql)
    result = cur.fetchone()
    return result

def GET_OWNERS():
    sql = "SELECT * FROM vehicleowner"
    cur.execute(sql)
    result = cur.fetchall()
    return result

def GET_ONWER_FROM_VEHICLE(np):
    sql = "SELECT * FROM vehicleowner INNER JOIN vehicledetails ON vehicleowner.cnic = vehicledetails.cnic where vehicledetails.numberPlate = '"+np+"'"
    cur.execute(sql)
    result = cur.fetchone()
    return result
     
#pdf generation breakpoint
def removeSpaces(s):
    return str(s.replace(" ",""))
def makePDF(data,img1,img2):
    
    ChallanNumber = removeSpaces(data[8]+getCurrentTime())
    NumberPlate = data[8]
    DateTime = getCurrentChallanDateTime()
    ViolationType = "Helmet"
    city = data[4]
    Province = data[6]
    Name = data[1]+" "+data[2]+" "+data[3]
    cnic = data[0]
    Phone = data[7]
    Address = data[5]
    VehicleRegYear = str(data[10])
    Make = data[11]
    color = data[12]
    ChasisNo = data[13]
    EngineNo = data[14]
    IssuedDateTime = getCurrentChallanDateTime()
    DueDate = getDueDate()
    ChallanAmount = "1490"
    
    
    pdf = canvas.Canvas("challans/"+ChallanNumber+".pdf")
    pdf.setTitle(ChallanNumber)
    
    
    
    DocumentTitle = "E-Challan"
    Document_Subtitle = "Precidium"
    
    pdf.setFont('Courier',25)
    pdf.drawCentredString(300,780,DocumentTitle)
    
    pdf.setFont('Courier',20)
    pdf.drawCentredString(300,760,Document_Subtitle)
    
    
    #logos
    precedium_logo = "logo2.jpg"
    pdf.drawImage(precedium_logo,30,740,120,80)
    
    
    #top right square
    pdf.line(300,750,565,750)
    pdf.line(300,700,565,700)
    pdf.line(300,750,300,700)
    pdf.line(565,750,565,700)
    
    pdf.setFont("Times-Roman",10)
    pdf.drawString(310,740,"Challan # ")
    pdf.drawString(380,740,ChallanNumber)
                   
    pdf.drawString(310,728,"Number Plate  ")
    pdf.drawString(380,728,NumberPlate)
    
    pdf.drawString(310,716,"Amount ")
    pdf.setFillColorRGB(255,0,0)
    pdf.drawString(400,716,str(ChallanAmount+" Rupees"))
    pdf.setFillColorRGB(0,0,0)
    pdf.drawString(310,704,"Due Date ")
    pdf.setFillColorRGB(255,0,0)
    pdf.drawString(400,704,DueDate)
    pdf.setFillColorRGB(0,0,0)
    
    #values
    
    
    #main table 
    
    pdf.line(30,697,30,300)
    pdf.line(565,697,565,300)
    pdf.line(30,697,565,697)
    pdf.line(30,300,565,300)
    
    
    pdf.line(30,675,565,675)
    pdf.line(30,620,565,620)
    pdf.line(30,560,565,560)
    
    pdf.line(30,515,565,515)
    pdf.line(30,450,565,450)
    pdf.line(30,405,565,405)
    
    #1st row
    pdf.setFont("Times-Roman",16)
    pdf.drawCentredString(320,680,"Violation / Vehical Details")
    
    
    #2nd row
    pdf.line(200,675,200,620)
    pdf.line(350,675,350,620)
    pdf.line(450,675,450,620)
    
    pdf.setFont("Times-Roman",10)
    pdf.drawString(40,660,"Date & Time of Offence")
    pdf.drawString(60,640,DateTime)
    
    pdf.drawString(210,660,"Voilation Type")
    pdf.drawString(220,640,ViolationType)
    pdf.drawString(360,660,"City")
    pdf.drawString(370,640,city)
    pdf.drawString(460,660,"Province")
    pdf.drawString(470,640,Province)
    
    
    #3rd row
    pdf.line(275,620,275,560)
    pdf.line(425,620,425,560)
    
    pdf.setFont("Times-Roman",10)
    pdf.drawString(40,605,"Name (first,middle,last)")
    pdf.drawString(60,585,Name)
    pdf.drawString(285,605,"Cnic")
    pdf.drawString(295,585,cnic)
    pdf.drawString(435,605,"Phone")
    pdf.drawString(445,585,Phone)
    
    
    #4th row
    pdf.setFont("Times-Roman",10)
    pdf.drawString(40,545,"Address")
    pdf.drawString(50,535,Address)
    
    
    #5th row
    pdf.line(150,515,150,450)
    pdf.line(250,515,250,450)
    pdf.line(350,515,350,450)
    pdf.line(450,515,450,450)
    
    
    pdf.setFont("Times-Roman",10)
    pdf.drawString(40,500,"Vehical Registration Year")
    pdf.drawString(50,480,VehicleRegYear)
    pdf.drawString(160,500,"Make")
    pdf.drawString(160,480,Make)
    pdf.drawString(260,500,"Color")
    pdf.drawString(270,480,color)
    pdf.drawString(360,500,"Chasis No.")
    pdf.drawString(370,480,ChasisNo)
    pdf.drawString(460,500,"Engine No.")
    pdf.drawString(470,480,EngineNo)
    
    
    
    #6th row
    pdf.line(250,450,250,405)
    
    pdf.setFont("Times-Roman",10)
    pdf.drawString(40,440,"Challan # ")
    pdf.drawString(50,425,ChallanNumber)
    pdf.drawString(260,440,"Issued Date & Time")
    pdf.drawString(270,420,IssuedDateTime)
    
    #7th row
    
    pdf.setFont("Times-Roman",10)
    pdf.drawString(40,380,"Notice")
    pdf.drawString(400,380,"Penality")
    pdf.setFillColorRGB(255,0,0)
    pdf.drawString(420,350,str(ChallanAmount+" Rupees"))
    
    #pictures
    image = "images/"+img1
    cropped = "np/"+img2
    pdf.drawImage(image,30,100,265,180)
    pdf.drawImage(cropped,300,100,265,180)
    
    pdf.save()    
    
#    sql = "INSERT INTO challan VALUES (%s,%s,%s,%s,%s,%s,%s,%s,,%s,%s,%s,%s,%s)"
    sql = "INSERT INTO challan VALUES (null,'"+data[8]+"','"+str(ChallanAmount)+"','"+DueDate+"','"+getDate()+"','"+getTime()+"','Helmet','Rawalpindi','Punjab','"+getDate()+"','"+getTime()+"','"+img1+"','"+img2+"')"
 #   value = ("null",data[8],str(ChallanAmount),DueDate,getDate(),getTime(),"Helmet","Rawalpindi","Punjab",getDate(),getTime(),img1,img2)
##    print(value)
    #sql = "INSERT INTO challan VALUES (null,'KL4ASGH 5270','1490','12-07-2020','2020-06-27','08:52:29 PM','Helmet','Rawalpindi','Punjab','2020-06-27','08:52:29 PM','bike.jpg','bike_20200627_205228.png')";   
    cur.execute(sql)
    mydb.commit()
    
def VEHICLE_DETAILS(o):    
    sql = "INSERT INTO vehicledetails VALUES (%s,%s, %s,%s,%s,%s,%s)"
    value = (o['numberPlate'],o['cnic'],o['regYear'],o['make'],o['color'],o['chasis'],o['engine'])
    cur.execute(sql,value)
    mydb.commit()
    
def DataofVehicle(plate,img1,img2):
    owners= GET_OWNERS()
    o = []
    for i in range(len(owners)):
        owner_cnic = owners[i][0]
        o.append(owner_cnic)
    owner = random.choice(o)
    reg = random.choice(years)
    color = random.choice(colors)
    make1 = random.choice(makes)
    chasis1 = random.choice(chasis)
    engine = random.choice(chasis)
    
    vehicle_data = {
        "numberPlate":plate,
        "cnic":owner,
        "regYear":reg,
        "make":make1,
        "color":color,
        "chasis":chasis1,
        "engine":engine
        }
    VEHICLE_DETAILS(vehicle_data)    
    DATABASE_MODULE(plate,img1,img2)
    
#Database main
def DATABASE_MODULE(number_plate,image1,image2):
    vehicle = GET_ONWER_FROM_VEHICLE(number_plate)
    print("Vehicle : ",vehicle) 
    if vehicle is None:
        DataofVehicle(number_plate,image1,image2)
    else:
        makePDF(vehicle,image1,image2)
    
#directory watch plus ocr breakpoint 


def OCR(image_path):    
    print("OCR MODULE STARTED AT",getCurrentChallanDateTime())
    
    blnKNNTrainingSuccessful = DetectChars.loadKNNDataAndTrainKNN()        

    if blnKNNTrainingSuccessful == False:                     
        print("\nerror: KNN traning was not successful\n") 
        return                                                          

    file =image_path
    imgOriginalScene  = cv2.imread(file)     

    if imgOriginalScene is None:                           
        print("\nerror: image not read from file \n\n")
        os.system("pause")                      
        return                                              
    

    listOfPossiblePlates = DetectPlates.detectPlatesInScene(imgOriginalScene)           # detect plates

    listOfPossiblePlates = DetectChars.detectCharsInPlates(listOfPossiblePlates)        # detect chars in plates

    #cv2.imshow("imgOriginalScene", imgOriginalScene)            # show scene image

    if len(listOfPossiblePlates) == 0:                          # if no plates were found
        print("\nno license plates were detected\n")  # inform user no plates were found
    else:                                                       # else
                # if we get in here list of possible plates has at leat one plate

                # sort the list of possible plates in DESCENDING order (most number of chars to least number of chars)
        listOfPossiblePlates.sort(key = lambda possiblePlate: len(possiblePlate.strChars), reverse = True)

                # suppose the plate with the most recognized chars (the first plate in sorted by string length descending order) is the actual plate
        licPlate = listOfPossiblePlates[0]

        #cv2.imshow("imgPlate", licPlate.imgPlate)           # show crop of plate and threshold of plate
        #cv2.imshow("imgThresh", licPlate.imgThresh)

        if len(licPlate.strChars) == 0:                     # if no chars were found in the plate
            print("\nno characters were detected\n\n")  # show message
            return                                          # and exit program
        # end if

        #drawRedRectangleAroundPlate(imgOriginalScene, licPlate)             # draw red rectangle around plate

        print("\nlicense plate read from image = " + licPlate.strChars + "\n")  # write license plate text to std out
        print("----------------------------------------")

        #writeLicensePlateCharsOnImage(imgOriginalScene, licPlate)           # write license plate text on the image

        #cv2.imshow("imgOriginalScene", imgOriginalScene)                # re-show scene image

        #cv2.imwrite("imgOriginalScene.png", imgOriginalScene)           # write image out to file

    # end if else

    #cv2.waitKey(0)					# hold windows open until user presses a key

    
        
    cv2.imwrite("np/"+licPlate.strChars+".png",licPlate.imgPlate)
    FullImageName = os.path.basename(file)
    NpCroopedName =licPlate.strChars+".png"
    np = str(licPlate.strChars)
    print("Number Is : ",np);
    print("OCR MODULE ENDED AT ",getCurrentChallanDateTime())
    DATABASE_MODULE(np,FullImageName,NpCroopedName)
    #cv2.waitKey(0)



def app():
    load_Module()   
    path_to_watch = ("images/")
    change_handle = win32file.FindFirstChangeNotification (
      path_to_watch,
      0,
      win32con.FILE_NOTIFY_CHANGE_FILE_NAME
    )
    print("Watching ",path_to_watch)
    try:
    
      old_path_contents = dict ([(f, None) for f in os.listdir (path_to_watch)])
      while 1:
        result = win32event.WaitForSingleObject (change_handle, 500)
        if result == win32con.WAIT_OBJECT_0:
          new_path_contents = dict ([(f, None) for f in os.listdir (path_to_watch)])
          added = [f for f in new_path_contents if not f in old_path_contents]
          deleted = [f for f in old_path_contents if not f in new_path_contents]
          if added: applyOcr(added[0])
          if deleted: print ("Deleted: ",deleted[0])
    
          old_path_contents = new_path_contents
          win32file.FindNextChangeNotification (change_handle)
        
    finally:
      win32file.FindCloseChangeNotification (change_handle)
    


def load_Module():
    t.sleep(1)
    print("Loaded")        
    t.sleep(1)
    print("DIRECTORY CHANGE CHECKING MODULE IS RUNNING TRY MAKING CHANGE WITHIN THE DIRECTORY")
    
    
def applyOcr(path_to_image):
    
    p1 = 'images/'+path_to_image
    print('Applying OCR ON THIS IMAGE ' ,p)
    OCR(p1)


if __name__=="__main__":
    
    app()


    
