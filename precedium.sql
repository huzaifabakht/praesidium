-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2020 at 04:15 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `precedium`
--

-- --------------------------------------------------------

--
-- Table structure for table `analysis`
--

CREATE TABLE `analysis` (
  `analysisId` int(11) NOT NULL,
  `result` varchar(500) NOT NULL,
  `redZone` varchar(500) NOT NULL,
  `analysis` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `challan`
--

CREATE TABLE `challan` (
  `challanId` int(255) NOT NULL,
  `numberPlate` varchar(255) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `dueDate` varchar(255) NOT NULL,
  `dateofOffence` varchar(255) NOT NULL,
  `timeofOffence` varchar(255) NOT NULL,
  `voilationType` text NOT NULL,
  `city` text NOT NULL,
  `province` text NOT NULL,
  `issueDate` varchar(255) NOT NULL,
  `issueTime` varchar(255) NOT NULL,
  `image1` varchar(300) NOT NULL,
  `image2` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `challan`
--

INSERT INTO `challan` (`challanId`, `numberPlate`, `amount`, `dueDate`, `dateofOffence`, `timeofOffence`, `voilationType`, `city`, `province`, `issueDate`, `issueTime`, `image1`, `image2`) VALUES
(7, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:13:23 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:13:23 AM', 'img3.jpg', 'bike_20200701_091312.png'),
(8, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:15:41 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:15:41 AM', 'bike.jpg', 'bike_20200701_091540.png'),
(9, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:19:00 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:19:00 AM', 'bike.jpg', 'bike_20200701_091900.png'),
(10, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:19:20 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:19:20 AM', 'bike.jpg', 'bike_20200701_091919.png'),
(11, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:19:24 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:19:24 AM', 'bike.jpg', 'bike_20200701_091924.png'),
(12, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:19:26 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:19:26 AM', 'bike.jpg', 'bike_20200701_091926.png'),
(13, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:19:28 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:19:28 AM', 'bike.jpg', 'bike_20200701_091928.png'),
(14, 'KL4ASGH 5270', '1490', '16-07-2020', '2020-07-01', '09:21:19 AM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-01', '09:21:19 AM', 'bike.jpg', 'bike_20200701_092118.png'),
(15, 'KL4ASGH 5270', '1490', '17-07-2020', '2020-07-02', '08:59:42 PM', 'Helmet', 'Rawalpindi', 'Punjab', '2020-07-02', '08:59:42 PM', 'bike - Copy.jpg', 'bike - Copy_20200702_205939.png');

-- --------------------------------------------------------

--
-- Table structure for table `challan_status`
--

CREATE TABLE `challan_status` (
  `challan_id` varchar(256) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `challan_status`
--

INSERT INTO `challan_status` (`challan_id`, `status`) VALUES
('7', 1),
('8', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users_data`
--

CREATE TABLE `users_data` (
  `Full_Name` varchar(20) NOT NULL,
  `Password` varchar(10) NOT NULL,
  `approval_status` tinyint(1) NOT NULL DEFAULT 0,
  `Role` int(3) NOT NULL,
  `Is_Delete` int(2) NOT NULL DEFAULT 0,
  `User_Id` int(255) NOT NULL,
  `Designation` varchar(10) NOT NULL,
  `Date_Created` date NOT NULL,
  `Contact_Number` varchar(14) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_data`
--

INSERT INTO `users_data` (`Full_Name`, `Password`, `approval_status`, `Role`, `Is_Delete`, `User_Id`, `Designation`, `Date_Created`, `Contact_Number`, `Email`, `Address`) VALUES
('admin', 'admin123', 1, 1, 0, 1, '1', '2020-03-11', '03345248399', 'zubairzameer77@gmail.com', ''),
('zubair zameer', 'admin123', 1, 2, 0, 2, '2', '0000-00-00', '03345248399', 'test@gmail.com', 'house no 112 lane 5 askari 10 rawalpindi'),
('manager', 'manager123', 1, 2, 0, 3, '2', '0000-00-00', 'asdasd', 'manager@gmail.com', 'asdad'),
('blah', 'blah', 0, 1, 0, 4, '1', '0000-00-00', 'blah', 'blah', 'blah');

-- --------------------------------------------------------

--
-- Table structure for table `vehicledetails`
--

CREATE TABLE `vehicledetails` (
  `numberPlate` varchar(255) NOT NULL,
  `cnic` varchar(255) NOT NULL,
  `regYear` text NOT NULL,
  `make` varchar(255) NOT NULL,
  `color` text NOT NULL,
  `chasis` varchar(255) NOT NULL,
  `engine` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicledetails`
--

INSERT INTO `vehicledetails` (`numberPlate`, `cnic`, `regYear`, `make`, `color`, `chasis`, `engine`) VALUES
('KL49H 5270', '3360592740201', '2012', 'HONDA', 'Royal Black', '3BK5-124090L', '3BK5-124090L'),
('KL4ASGH 5270', '3360592740201', '2012', 'HONDA', 'Royal Black', '3BK5-124090L', '3BK5-124090L'),
('RIK 2601', '35202968915611', '2013', 'SUZUKI', 'RED', '1231233', '1122');

-- --------------------------------------------------------

--
-- Table structure for table `vehicleowner`
--

CREATE TABLE `vehicleowner` (
  `cnic` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `middleName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `city` text NOT NULL,
  `address` varchar(300) NOT NULL,
  `province` text NOT NULL,
  `phone` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `vehicleowner`
--

INSERT INTO `vehicleowner` (`cnic`, `firstName`, `middleName`, `lastName`, `city`, `address`, `province`, `phone`) VALUES
('3360592740201', 'Mr. Afzaal', '', 'Chaudhary', 'Islamabad', 'IT Centre Sector G-10/1', 'Federal', '+923312621005'),
('35202968915611', 'zubair', 'zameer', 'shah', 'rawalpindi', 'house no 112 lane 5 askari 10 ', 'punjab', '03345248399');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analysis`
--
ALTER TABLE `analysis`
  ADD PRIMARY KEY (`analysisId`);

--
-- Indexes for table `challan`
--
ALTER TABLE `challan`
  ADD PRIMARY KEY (`challanId`,`numberPlate`),
  ADD KEY `numberPlate` (`numberPlate`);

--
-- Indexes for table `challan_status`
--
ALTER TABLE `challan_status`
  ADD UNIQUE KEY `challan_id` (`challan_id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_data`
--
ALTER TABLE `users_data`
  ADD UNIQUE KEY `User_Id` (`User_Id`);

--
-- Indexes for table `vehicledetails`
--
ALTER TABLE `vehicledetails`
  ADD PRIMARY KEY (`numberPlate`,`cnic`),
  ADD KEY `cnic` (`cnic`);

--
-- Indexes for table `vehicleowner`
--
ALTER TABLE `vehicleowner`
  ADD PRIMARY KEY (`cnic`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analysis`
--
ALTER TABLE `analysis`
  MODIFY `analysisId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `challan`
--
ALTER TABLE `challan`
  MODIFY `challanId` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users_data`
--
ALTER TABLE `users_data`
  MODIFY `User_Id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `challan`
--
ALTER TABLE `challan`
  ADD CONSTRAINT `challan_ibfk_1` FOREIGN KEY (`numberPlate`) REFERENCES `vehicledetails` (`numberPlate`);

--
-- Constraints for table `vehicledetails`
--
ALTER TABLE `vehicledetails`
  ADD CONSTRAINT `vehicledetails_ibfk_1` FOREIGN KEY (`cnic`) REFERENCES `vehicleowner` (`cnic`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
